.PHONY: all clean

%.html: %.rst
	rst2html5.py $< public/$@

all: $(patsubst %.rst, %.html, $(wildcard *.rst))

clean:
	-rm -f public/*.html
