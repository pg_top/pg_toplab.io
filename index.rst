=================================================
 Welcome to the PostgreSQL top Project Home Page
=================================================

.. image:: pg_top.png
   :width: 75px
   :align: center

----

pg_top is 'top' for PostgreSQL.  It is derived from `Unix Top
<https://web.archive.org/web/20170521033224/http://www.unixtop.org/>`_.
Similar to top, pg_top allows you to monitor PostgreSQL processes.  Also see
`pg_systat <https://pg_systat.gitlab.io/>`_ to monitor various PostgreSQL
statistics tables.

pg_top also allows you to:

* View currently running SQL statement of a process.
* View query plan of a currently running SQL statement.
* View locks held by a process.
* Monitor remote databases that have the `pg_proctab
  <https://pg_proctab.gitlab.io/>`_ extension loaded.

.. image:: pg_top-2.png
   :width: 800px

Other `screenshots <screenshots/>`_ and some instruction using this
application.

Download current releases:

.. list-table::
   :header-rows: 1

   * - Version
     - Download
     - Checksum
     - Release date
   * - 4.1.1
     - `.AppImage <appimage/pg_top-4.1.1-x86_64.AppImage>`_
     - `md5 <appimage/pg_top-4.1.1-x86_64.AppImage.md5>`__
       `sha256  <appimage/pg_top-4.1.1-x86_64.AppImage.sha256>`__
     - 2024-06-06
   * -
     - `.xz <source/pg_top-4.1.1.tar.xz>`_
     - `md5 <source/pg_top-4.1.1.tar.xz.md5>`__
       `sha256  <source/pg_top-4.1.1.tar.xz.sha256>`__
     - `Changes <https://gitlab.com/pg_top/pg_top/-/blob/main/HISTORY.rst>`_

`View the source code on-line <https://gitlab.com/pg_top/pg_top>`_.

Download the source code with `git <https://www.git-scm.com/>`_::

    git clone git@gitlab.com:pg_top/pg_top.git

The PostgreSQL top project is a `PostgreSQL <https://www.postgresql.org>`_
Community project.
